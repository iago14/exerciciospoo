//Exercicio 20 Repeti��o e/ou Sele��o 

import java.util.Scanner;

public class Exe4 {
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int idade = 1;
		int cont = 0;
		double media = 0;
		
		while(idade != 0){
			
			System.out.println("Digite sua idade: ");
			idade = tecla.nextInt();
			
			if(idade != 0){
				media = media + idade;
				cont++;
			} 
				
		}
		if(cont > 0){
			System.out.println("A media de idade � " + media / cont);
			System.out.println("A quantidade de pessoas � " + cont);
		} else {
			System.out.println("N�o tem idade");
		}
		
	}
	
}

