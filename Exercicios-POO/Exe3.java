//Exercicio 09 Repeti��o e/ou Sele��o

import java.util.Scanner;

public class Exe3 {
	static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {

		double nota1, nota2, media;
		int resp = 0;
		int cont = 0;

		while (resp <= 1) {
			System.out.println("Digite a nota da 1 avaliacao ");
			nota1 = tecla.nextDouble();

			System.out.println("Digite a nota da 2 avaliacao ");
			nota2 = tecla.nextDouble();
			
			media = (nota1 + nota2) / 2;
			
			if(media >= 6.0){
				cont+=1;
			}
			
			System.out.println("Calcular a media de outro aluno? 1.Sim 2.Nao ");
			resp = tecla.nextInt();
		}
		System.out.println("Quantidade de alunos aprovados: " + cont);
	}

}
