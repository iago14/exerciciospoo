//Exercicio 01 - Simples

import java.util.Scanner;

public class Exe1 {
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("Digite o raio do circulo: ");
		double raio = tecla.nextDouble();
		
		double area = 3.14 * (raio * raio);
		System.out.println("Area = " + area);
	}

}
