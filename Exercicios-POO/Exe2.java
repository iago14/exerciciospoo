//Exercicio 05 - Sele��o

import java.util.Scanner;

public class Exe2 {
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.print("Digite um numero: ");
		int numero = tecla.nextInt();
		
		if(numero >= 0)
			System.out.println("Valor digitado � positivo");
		else
			System.out.println("Valor digitado � negativo");
	}

}
