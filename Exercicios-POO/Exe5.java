//Exercicio 26  Repeti��o e/ou Sele��o 

import java.util.Scanner;

public class Exe5 {
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int numero;
		
		System.out.println("Digite um numero de 1 a 9: ");
		numero = tecla.nextInt();
		
		for(int contador = 0; contador <= 10; contador++){
				System.out.print(numero * contador);
				System.out.print(" ");
			}
			System.out.println();
		}

}
