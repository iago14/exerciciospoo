package Exercicio02;
public class Conta {

    private final static double TAXA = 8.9;
    
    static int contador = 0;
    
    //Atributos de inst�ncia
    private int numero;
    private double saldo;
    private boolean status;
    
    //M�todos de acesso
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    //Construtor
    public Conta(int numero, double saldo) {
        this.numero = numero;
        this.saldo = saldo;
        this.status = true;
    }
    
    //Sobrecarregado
    public Conta(double saldo){
    	this.numero = ++contador; //pre-incremento
        this.saldo = saldo;
        this.status = true;
    }

    public boolean sacar(double valor){
        
        if(this.saldo >= valor){
        	this.saldo -= valor;
        	return true;
        } else {
        	return false;
        }
        //saldo = saldo - valor;
    }
    
    public void depositar(double valor){
        this.saldo += valor;
        //saldo = saldo + valor;
    }
    
    public boolean transferir(double valor, int conta, int contaTransferir){
    	if(this.sacar(valor)){
    		conta.deposita(valor);
    		return true;
    	} else {
    		return false;
    	}
    }
}





