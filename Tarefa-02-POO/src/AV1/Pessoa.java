package AV1;

public class Pessoa implements Comparable<Pessoa> {
	
	static int contador = 0;
	
	protected int id;
	protected String nome;
	protected int idade;
	
	/*public Pessoa(int id, String nome, int idade) {
		this.id = id;
		this.nome = nome;
		this.idade = idade;
	}*/
	
	public Pessoa(String nome, int idade) {
		this.id = ++contador;
		this.nome = nome;
		this.idade = idade;
	}

	public Pessoa() {
		// TODO Auto-generated constructor stub
	}

	public Pessoa(int id, String nome, int idade) {
		this.id = id;
		this.nome = nome;
		this.idade = idade;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	@Override
	public int compareTo(Pessoa o) {
		// TODO Auto-generated method stub
		if (this.idade < o.getIdade()) {
			return -1;
		}
		if (this.idade > o.getIdade()) {
			return 1;
		}
		return 0;

	}
	
	public void print(){}
	
	
}
