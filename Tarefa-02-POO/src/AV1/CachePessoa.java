package AV1;
import java.util.*;

public class CachePessoa {
	
	static List<Pessoa> listaCache = new ArrayList<Pessoa>();
	
	public void addPessoa(int id, String nome, int idade) {
		listaCache.add(new Pessoa(id, nome, idade));
	}
	
	
	public Pessoa retornaPessoa(int id) {
		for (Pessoa item : listaCache) {
			if (item.getId() == id){
				return item;
			} 
		}
		return null;	
	}
	
}
	
	
/*
	public void retornaPessoa(int valor) {

		if (Aplicativo.listaCache.contains(valor)) {
			System.out.println("ta no cache filhote");

		}

		else {
			for (Pessoa item : Aplicativo.lista) {
				if (item.getId() == valor) {
					Aplicativo.listaCache.add(new CachePessoa(valor, item.getNome(), item.getIdade()));
					System.out.println(item.getNome() + "........" + item.getIdade());	
				}
			}

		}
	}

	public void print() {
		System.out.println(getNome() + "\t" + getIdade() + "\t" + getId());
	} */


