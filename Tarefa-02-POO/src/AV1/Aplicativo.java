package AV1;

import java.util.*;


public class Aplicativo {
	
    static List<Pessoa> lista = new ArrayList<Pessoa>();
    
    static CachePessoa cache = new CachePessoa();
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		 int op;
	        do {                
	            System.out.println("*** MENU PRINCIPAL ***");
	            System.out.println("1-Cadastrar Pessoa");
	            System.out.println("2-Ordenar");
	            System.out.println("3-Listar Cadastros");
	            System.out.println("4-Buscar CACHE");
	            System.out.println("5-Buscar por idade");
	            System.out.println("6-Sair");
	            System.out.println("Digite sua op��o: ");
	            op = tecla.nextInt(); 
	            switch(op){
	                case 1: cadastrarPessoa(); break;
	                case 2: ordenarLista(); break;
	                case 3: listarCadastro(); break;    
	                case 4: buscarCadastro(); break;
	                case 5: buscarIdade(); break;
	                case 6: break;
	            }
	        } while (op!=6);       
	}
	
	public static void cadastrarPessoa() {
		System.out.println("Digite o nome: ");
		String nome = tecla.next();
		
		System.out.println("Digite sua idade: ");
		int idade = tecla.nextInt();
		
		lista.add(new Pessoa(nome, idade));
		System.out.println("Cadastro realizado com sucesso!");
		
	}
	
	public static void listarCadastro() {
		
		for(Pessoa item : lista){
			System.out.println(item.getNome()
					+ "........" +
					item.getIdade());
		}
	}
	
	public static void ordenarLista() {
		Collections.sort(lista);
		listarCadastro();
	}
	
    public static void imprimir(int id) {
    	CachePessoa obj = new CachePessoa();
    	obj.retornaPessoa(id);
    }
    
    
    public static void buscarIdade(){
		System.out.println("Digite a idade que deseja buscar: ");
		int idade = tecla.nextInt();
    	
    	for(Pessoa item : lista){
    		if (item.getIdade() == idade) {
    			System.out.println(item.getNome()
    					+ "........" +
    					item.getIdade());
    		} else {
    			System.out.println("N�o encontrado.");
    		}

		}
    	
    }
    
    
	
	public static void buscarCadastro() {
		System.out.println("Digite o id para buscar: ");
		int id = tecla.nextInt();
		Pessoa itemp = null;
		
		if (cache.retornaPessoa(id) != null) {
			itemp = cache.retornaPessoa(id);
		} 
		
		else {
			for (Pessoa item : lista) {
				if (item.getId() == id) {
					itemp = item;
					cache.addPessoa(item.getId(), item.getNome(), item.getIdade());
				}
			}
		}
		
		if(itemp != null){
			System.out.println(itemp.getId()+"......."+itemp.getNome()+"......."+itemp.getIdade());
		}
	

	}
}
