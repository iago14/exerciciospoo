package Exercicio01;

import java.util.Scanner;

public class APP {
	
	static final int MAXPESSOA = 20;
	
    static int index = 0;
	
	static Pessoa[] lista = new Pessoa[MAXPESSOA];
	
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir Pessoa");
            System.out.println("2-Listar Pessoa");
            System.out.println("3-Sair");
            System.out.println("Digite sua opção: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirPessoa(); break;
                case 2: listarPessoa(); break;
                case 3: break;
            }
        } while (op!=3);       
    }

    public static void incluirPessoa(){
    	
    	System.out.println("Digite seu nome: ");
    	String nome = tecla.next();
    	
    	System.out.println("Digite sua idade: ");
    	int idade = tecla.nextInt();
    	

        lista[index++] = new Pessoa(nome, idade);
        System.out.println("Conta cadastrada com sucesso!");
    	
    }
    
    public static void listarPessoa(){
    	double total = 0;
    	
    	for (int i = 0; i < lista.length - 1; i++){
    		if (lista[i] != null){
    			System.out.println("Nome: " + lista[i].getNome());
    		} else {
    			break;
    		}
    	}
    }

}



