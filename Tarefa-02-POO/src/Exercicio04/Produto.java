package Exercicio04;

public class Produto {
	
	String nome;
	double preco;
	int codigoDoProduto;
	int quantidade;
	
	//Construtor
	public Produto(String nome, double preco, int codigoDoProduto, int quantidade){
		this.nome = nome;
		this.preco = preco;
		this.codigoDoProduto = codigoDoProduto;
		this.quantidade = quantidade;
	}
	
	public int getCodigoDoProduto(){
		return codigoDoProduto;
	}
	
	public int getQuantidade(){
		return quantidade;
	}
	
	public String getNome(){
		return nome;
	}
	
	
	public void retirar(int codigoDoProduto){
		if (this.quantidade <= 0){
			System.out.println("N�o � possivel realizar retirada");
		} else {
			this.quantidade -= 1;
		}
	}
}

