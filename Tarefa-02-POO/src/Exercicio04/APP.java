package Exercicio04;

import java.util.Scanner;

public class APP {
	
	static final int MAX = 10;
	
	static int index = 0;
	
	static Produto[] lista = new Produto[MAX];
	
	static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		 int op;
	        do {                
	            System.out.println("*** MENU PRINCIPAL ***");
	            System.out.println("1-Incluir Produto");
	            System.out.println("2-Retirar Produto");
	            System.out.println("3-Listar Produtos");
	            System.out.println("4-Verificar produtos baixo estoque");
	            System.out.println("5-Sair");
	            System.out.println("Digite sua opção: ");
	            op = tecla.nextInt(); 
	            switch(op){
	                case 1: incluirProduto(); break;
	                case 2: retirarProduto(); break;
	                case 3: listarProduto(); break; 
	                case 4: verificarProduto(); break;
	                /*case 4: listarContas(); break;
	                case 5: break; */
	            }
	        } while (op!=5);       
	}
	
	public static void incluirProduto(){
		
		System.out.println("Digite o nome do produto:");
		String nome = tecla.next();
		
		System.out.println("Digite o preco:");
		int preco = tecla.nextInt();
		
		System.out.println("Digite o codigo:");
		int codigoDoProduto = tecla.nextInt();
		
		System.out.println("Digite a quantidade:");
		int quantidade = tecla.nextInt(); 
		
		lista[index++] = new Produto(nome, preco, codigoDoProduto, quantidade);
		System.out.println("Produto cadastrado!");
		
		
	}
	
	public static void retirarProduto(){
		System.out.println("Digite o codigo do produto que deseja retirar:");
		int codigoDoProduto = tecla.nextInt();
		
		for (int i = 0; i < lista.length-1; i++) {
			if (codigoDoProduto == lista[i].getCodigoDoProduto()){
				lista[i].retirar(codigoDoProduto);
				System.out.println("Produto retirado");
				break;
			}
		}
	}
	
	
	public static void listarProduto(){
		for (int i = 0; i < lista.length-1; i++) {
			if (lista[i] != null){
				System.out.println(lista[i].getNome());
				System.out.println(lista[i].getCodigoDoProduto());
				System.out.println(lista[i].getQuantidade());
			} else {
				break;
			}
		}
	}
	
	public static void verificarProduto(){
		
		for (int i = 0; i < lista.length-1; i++) {
			int quantidade = lista[i].getQuantidade();
			if (lista[i] != null && quantidade <= 0){
				System.out.println("Nome do produto: " + lista[i].getNome());
				System.out.println("Quantidade: " + lista[i].getQuantidade());
				break;
			}
		}
	}

}
