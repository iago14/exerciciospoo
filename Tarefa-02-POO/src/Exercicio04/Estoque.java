package Exercicio04;

public class Estoque {
	
	int quantidade;
	int minQuantidade;
	
	public Estoque(int quantidade, int minQuantidade){
		this.quantidade = quantidade;
		this.minQuantidade = minQuantidade;
	}
	
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public int getMinQuantidade() {
		return minQuantidade;
	}
	public void setMinQuantidade(int minQuantidade) {
		this.minQuantidade = minQuantidade;
	}
}
